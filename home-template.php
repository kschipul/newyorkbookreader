<?php
//we don't want anyone to access this file directly
if( strstr($_SERVER["REQUEST_URI"], basename(__FILE__) ) ){
	header("Location: ./");
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="A tool to find Craigslist books in the NYC area" />
    <meta name="author" content="Karl Schipul" />

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous" />

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet" />
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
	integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	crossorigin="anonymous"></script>
  </head>

  <body class="text-center">
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	  <a class="navbar-brand" href="#">Craigslist NYC Book Search</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse"
	  data-target="#navbarNav" aria-controls="navbarNav"
	  aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
		  <li class="nav-item active">
			<a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="./get_cl">Update local database from Craigslist source...</a>
		  </li>
		</ul>
	  </div>
	</nav>
  
    <form class="form-signin" action="./search">
      <img class="mb-4 border"
	  src="http://aptgadget.com/wp-content/uploads/2016/12/Craigslist.jpg"
	  width="auto" height="200" />

      <h1 class="h3 mb-3 font-weight-normal">Search Craigslist books in NYC area...</h1>
      <input type="text" id="searchTerms" name="search_term" class="form-control"
	  placeholder="search term..." value="<?php echo $search_term ?>" required autofocus />
      <button class="btn btn-lg btn-primary btn-block" type="submit">Search</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>

	<?php
	//if there are some actual search results
	if( isset($gss_arr) && count($gss_arr) > 0 ){
		
		echo '<h2>RESULTS</h2><h5>total results: ' . count($gss_arr) . '</h5>';
		
		if( $page_count > 1 ){
		?>
			<form action="./search" id="paginator_form" method="get">
			<h4>page
			<input id="paginator_input" type="number" name="p" value="<?php echo $page_num ?>"
			min="1" max="<?php echo $page_count ?>" />
			<input type="hidden" name="search_term" value="<?php echo $search_term ?>" />
			of <?php echo $page_count ?></h4>
			
			<button class="btn btn-sm btn-primary" type="submit">Go</button>
			</form>
		<?php
		}//end if
		
		echo '<ul class="list-group">';
		
		//first member of array for this page
		$start_at = ($page_num - 1) * $max_results_per_page;
		
		//last member of array for this page
		$max_num_results_this_page = min(count($gss_arr), $max_results_per_page );
		
		$end_at = min( ($start_at + $max_num_results_this_page) , count($gss_arr));

		for($i=$start_at; $i< $end_at; $i++){
			$result = $gss_arr[$i];
		?>
		
		<li class="list-group-item">
			<h3>
				<a href="<?php echo $result["url"] ?>">
				<?php echo $result["title"] ?>
				</a>
			</h3>
			<h4><?php echo $result["url"] ?></h4>
			
			<h5>price: $<?php echo $result["price"] ?></h5>
		</li>
		<?php
		}//end for($i...)
		
		echo '</ul>';
	}//end if
	?>
	
	<br/><br/>
  </body>
</html>