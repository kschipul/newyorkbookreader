# README #

This is an excercise on how to make a derivative app off of a big name organization.  In this case, it is Craigslist.  Specifically, the objective here is to acquire and present search results of Books within the geographic New York City area.

### Features: data acquisition ###

* no more than 1000 results acquired from Craigslist per session.
* scope of what is acquired is limited to searches of books from within the New York City area.
* data is initially acquired via CURL requests initiated by PHP Script
* If a particular CURL request session requires more than one page of results (up to a total of 1000), then there will be a gentle, javascript based pagination for subsequent CURL data grabs.
* Relies on PHPQuery for transforming raw HTML page string content into usable DOM content, which is then turned into PHP arrays.
* The following data is collected from each Craigslist product: title, price, product page URL.
* Database used for saving data: AWS Elastic Beanstalk RDS

### Features: Search Engine UI ###

* All search requests point to a mySQL database on my own AWS Elastic Beanstalk RDS instance.
* All PHP files and UI files exist on an AWS Elastic Beanstalk PHP instance.  There are no secondary or assisting services other than the RDS database.  All work is done on this EB instance.
* Search functionality only reads from the aforementioned database, not live Craigslist CURL scans.
* As a bonus, Bootstrap is thrown in because almost everything is better when adding it, just like Chipotle sauce.

### More: ###

* There are only 3 php files, the big class file, the PHPQuery helper file and also the template PHP file for the UI.
* routing is handled with a combination of the php class file and also a .htaccess file.

### Live Project ###

* http://bookreaderphp-env.rixmnmk3hu.us-west-2.elasticbeanstalk.com/newyorkbookreader/
