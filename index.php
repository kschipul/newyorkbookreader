<?php

class CraigsListNewYorkBookLibrarian{
	
	//the book info original source. may have something like '?s=240' at the end, with the 's' variable is always multiple of 120
	private $base_url = "https://newyork.craigslist.org/search/bka";

	//hard constants. first is how many maximum results we ever want from a session.
	private $max_results = 1000;

	//second is how many results that craigslist displays per its own pages (max, can be less if the total is too small)
	private $cl_results_pp = 120;

	//the number that the final page will start at.  Will be 960 if above values are 1000 and 120. But for now is zero, because it is a class property (only applies to getting from craigslist resource operations)
	private $final_page_start = 0;
	
	//which craigslist page from the overall resource are we on? (only applies to getting from craigslist resource operations)
	private $current_cl_src_page = 0;
	
	//which page of our own search results is this on? (only applies to search form results from our db)
	private $current_results_page = 0;
	
	//how many results should there be for pagination on UI search operation?
	private $max_results_per_page = 20;

	//core table of the app
	private $tableName = "newyork_books";

	//is this currently on an aws ec2 server or not? typically can also be a localhost run.
	private $is_aws = false;
	
	//the database instance
	private $pdo_obj = null;
	
	//get hooked up to the right database
	function __construct(){
		
		//finalize a core class property.  only used on mysql inserts from craigslist.
		$this->final_page_start = intval($this->max_results / $this->cl_results_pp) * $this->cl_results_pp;

		//if this is the actual aws server, below lines will rely on those db settings
		if( strstr($_SERVER["HTTP_HOST"], "elasticbeanstalk.com") ){
			$this->is_aws = true;
		}else{
			//contains settings for local test db.  Now needed since this is not on AWS in this scenario.
			$config_file_contents = file_get_contents('./config.json');
			$config_mysql_json = json_decode( $config_file_contents );
		}

		//db settings
		$host = ( $this->is_aws )? $_SERVER['RDS_HOSTNAME'] : $config_mysql_json->host;
		$db   = ( $this->is_aws )? $_SERVER['RDS_DB_NAME']  : $config_mysql_json->dbname;
		$user = ( $this->is_aws )? $_SERVER['RDS_USERNAME'] : $config_mysql_json->user;
		$pass = ( $this->is_aws )? $_SERVER['RDS_PASSWORD'] : $config_mysql_json->password;
		$charset = ( $this->is_aws )? 'utf8' : 'latin1';
		
		
		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];

		//do the db hookup
		try {
			$this->pdoObj = new PDO($dsn, $user, $pass, $options);
		} catch (\PDOException $e) {
			throw new \PDOException($e->getMessage(), (int)$e->getCode());
		}
		
		//if the 'cls' parameter is populated in a cls update request
		$this->current_cl_src_page = isset($_GET["cls"]) && $_GET["cls"]? intval($_GET["cls"]) : $this->current_cl_src_page;
		
		//this is what decides what happens based on what the url says
		$this->route();
	}
	
	private function route(){
		$current_php_dir = dirname( $_SERVER["PHP_SELF"] );

		//determine the endpoint
		$route_path_with_qs = str_replace( $current_php_dir . "/", "", $_SERVER["REQUEST_URI"]);
		
		//built to handle potential query strings
		$route_path = preg_replace("/\?((.)+)?/", "", $route_path_with_qs);

		//handle the endpoint
		switch($route_path){
			case "get_cl":
				//when someone wants to update database from live craigslist endpoint
				
				//get the curl contents
				$cl_contents = $this->getCurledPage();
				
				/*
				turn that html into an array to be inserted into the db.
				NOTE: if '$this->current_cl_src_page' == '$this->final_page_start',
				then the maximum amount in the returning array will be smaller so
				that the total amount of data inserted from all pages in this session
				will never be greater than '$this->max_results'
				*/
				$ins_arr = $this->getDataFromCurlHtml($cl_contents);
				
				//database insertion
				$this->insertBooks($ins_arr);

				//if this is anything other than the final page for inserts
				if( $this->current_cl_src_page < $this->final_page_start ){
					
					//what is the number for the next page?
					$next_start = $this->current_cl_src_page + $this->cl_results_pp;

					//relative URL to be redirected to
					$next_page = "{$current_php_dir}/{$route_path}?cls={$next_start}";

					//then redirect to the next page
					echo "redirecting to '{$next_page}' shortly...
					<script>
					window.setTimeout(function(){
					window.location = '{$next_page}';
					}, 5000);
					</script>";
				}else{
					echo "batch Craigslist results to database job complete<br/>
					redirecting to search...
					<script>
					window.setTimeout(function(){
					window.location = '{$current_php_dir}';
					}, 5000);
					</script>";
				}
			
				break;
			case "search":
				//search form request
				
				$search_term = "";
				
				//for populating the search box if someone did a search
				if( isset($_GET["search_term"]) && strlen($_GET["search_term"]) > 0 ){
					$search_term = $_GET["search_term"];
				}else{
					//redirect if there are no search terms in query
					header( "Location: ./" );
				}
				
				//array from database
				$gss_arr = $this->getSearchSelect($search_term);
				
				//how many total results?
				$csa = count($gss_arr);
				
				
				//how many total pages shall there be for pagination?
				$page_count = 1;
				
				//current page of this?
				$page_num = (isset($_GET["p"]) && is_numeric($_GET["p"]) && intval($_GET["p"]) > 0 && $_GET["p"] < $csa )? $_GET["p"] : 1;
				
				//only when the total results exceed the total results for page property
				if( $csa > $this->max_results_per_page ){
					$page_count = $csa / $this->max_results_per_page;
					
					//needs to round up to nearest integer
					$page_count = ceil( $page_count );
				}
				
				$max_results_per_page = $this->max_results_per_page;
				
				require_once("home-template.php");
			
				break;
			default:
				$search_term = "";
			
				//when someone visits the page randomly without any specific parameters
				require_once("home-template.php");
			
				break;
		}
	}
	
	/**
	 * A custom function that automatically constructs a multi insert statement.
	 * 
	 * @param string $tableName Name of the table we are inserting into.
	 * @param array $data An "array of arrays" containing our row data.
	 * @param PDO $pdo_object Our PDO object.
	 * @return boolean TRUE on success. FALSE on failure.
	 */
	function pdoMultiInsert($tableName, $data, $pdo_object){
		
		//Will contain SQL snippets.
		$rowsSQL = array();

		//Will contain the values that we need to bind.
		$toBind = array();
		
		//Get a list of column names to use in the SQL statement.
		$columnNames = array_keys($data[0]);

		//Loop through our $data array.
		foreach($data as $arrayIndex => $row){
			$params = array();
			foreach($row as $columnName => $columnValue){
				$param = ":" . $columnName . $arrayIndex;
				$params[] = $param;
				$toBind[$param] = $columnValue; 
			}
			$rowsSQL[] = "(" . implode(", ", $params) . ")";
		}

		//Construct our SQL statement
		$sql = "INSERT IGNORE INTO `$tableName` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

		//Prepare our PDO statement.
		$pdoStatement = $pdo_object->prepare($sql);

		//Bind our values.
		foreach($toBind as $param => $val){
			$pdoStatement->bindValue($param, $val);
		}
		
		//Execute our statement (i.e. insert the data).
		return $pdoStatement->execute();
	}
	
	/**
	 * takes data from the curl request and puts it up on the db
	 * 
	 * @param array $data An "array of arrays" containing our row data.
	 * @return boolean TRUE on success. FALSE on failure.
	 */
	function insertBooks($data){
		return $this->pdoMultiInsert($this->tableName, $data, $this->pdoObj);
	}
	
	//gets the raw content from the Craigslist page we are scanning via CURL
	//function getCurledPage($suffix = null){
	function getCurledPage(){
		$get_url = $this->base_url;
		
		//sometimes the url should have an 's' parameter for subsequent pages
		$get_url .= ( $this->current_cl_src_page != 0 )? "?s=" . $this->current_cl_src_page : "";

		// Get cURL resource
		$curl = curl_init();
		
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $get_url,
			CURLOPT_USERAGENT => 'Codular Sample cURL Request'
		));
		
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		
		// Close request to clear up some resources
		curl_close($curl);
		
		return $resp;
	}
	
	
	//accepts html content as first argument.  Returns product data from said html to be inserted.
	function getDataFromCurlHtml($markup, $page=0){
		
		//very cool library.  Imitates jQuery, but for PHP.
		require_once("phpQuery-onefile.php");
		
		//new php doc generated from html string
		$doc = phpQuery::newDocumentHTML($markup);

		//makes '$doc' into active object
		phpQuery::selectDocument($doc);

		//this is what gets returned
		$insArr = array();

		//loop through each of the DOM products
		phpQuery::each(pq('.result-row .result-info'),
		function ($Index, $Element) use(&$insArr, $page) {
			
			//where does the craigslist product listing link to? (some product display page)
			$insArr[$Index]['url'] = pq($Element)->find('a.result-title')->attr('href');
			
			//product title
			$insArr[$Index]['title'] = pq($Element)->find('a.result-title')->text();
			
			//product price
			$temp_price = pq($Element)->find('.result-meta .result-price')->text();
			$temp_price = $temp_price ? str_replace("$", "", $temp_price) : 0;
			
			$insArr[$Index]['price'] = intval($temp_price);
			
			//when was this record inserted? (UNIX numerical timestamp)
			$insArr[$Index]['time_insert'] = time();
			
			//which page of results did this originate from?
			$insArr[$Index]['orig_page'] = $page;
		});


		//when this is the final page, or for some reason, greater, limit the number of results
		if( $this->current_cl_src_page >= $this->final_page_start ){
			$max_results_page = $this->max_results - $this->final_page_start;

			$chunkedArr = array_chunk($insArr, $max_results_page);

			$insArr = $chunkedArr[0];
		}
		
		return $insArr;
	}
	
	/**
	 * generates the table if needed
	 * 
	 * @param string $tableName Name of the table we are inserting into.
	 * @param PDO $pdo_object Our PDO object.
	 * @return boolean TRUE on success. FALSE on failure.
	 */
	function generateBookTable($pdo_object){
		$tableName = $this->tableName;
		
		$returnVal = false;
		
		try {
			$sql = "CREATE TABLE `$tableName` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `title` varchar(255) NOT NULL,
			  `price` float DEFAULT NULL,
			  `url` varchar(255) NOT NULL,
			  `time_insert` int(11) NOT NULL,
			  `orig_page` int(4) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `url_UNIQUE` (`url`)
			) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;";
			
			$pdo_object->exec($sql);
			print("Created $tableName Table.\n");
			
			$returnVal = true;
		} catch(PDOException $e) {
			echo $e->getMessage();//Remove or change message in production code
		}
		
		return $returnVal;
	}
	
	function getSearchSelect($search_term){
		$qry_str = 'SELECT id, title, price, url FROM ' . $this->tableName . ' WHERE title LIKE \'%'. $search_term .'%\'';
		
		$stmt = $this->pdoObj->query( $qry_str );
		
		$return_arr = array();
		
		while ($row = $stmt->fetch()){
			//echo $row['title'] . "\n";
			
			$return_arr[] = $row;
		}
		
		return $return_arr;
	}
	
	function getTestSelect(){
		$stmt = $this->pdoObj->query('SELECT * FROM ' . $this->tableName );
		
		while ($row = $stmt->fetch()){
			echo $row['title'] . "\n";
		}
	}
}


$poppaDoc = new CraigsListNewYorkBookLibrarian();

//$poppaDoc->getDataFromCurlHtml();
